import React, { useState } from "react";
function UseStateFunction() {
    const initialTimer=2;
    //2:timer初始值
    const [timer, setTimer] = useState(initialTimer);
    //setTimer接受的是一个事件对象
    const onX = () => {
        setTimer(timer * 2);
    }
    const onMinus = () => {
        setTimer(timer - 1);
    }
    const onReset = () => {
        setTimer(initialTimer);
    }
    /*
    每次重复执行setInterval
    延时执行setTimeout【每次执行都会有一个延迟，多一次重复渲染】
    */
    // setTimeout(() => {
    //     setTimer(initialTimer+1);
    // },1000);
    return (<>
        <button onClick={onX}>乘2</button>
        <button onClick={onMinus}>减一</button>
        <button onClick={onReset}>重置</button>
        <div>time: {timer}</div>
    </>);
}
export default UseStateFunction;