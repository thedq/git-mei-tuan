import React, { useState } from "react";
function UseStateFunction() {
    const initialTimer=2;
    //2:timer初始值
    const [timer, setTimer] = useState(initialTimer);
    //setTimer接受的是一个事件对象
    const onX = () => {
        setTimer(timer * 2);
    }
    const onMinus = () => {
        setTimer(timer - 1);
    }
    const onReset = () => {
        setTimer(initialTimer);
    }
    return (<>
        <button onClick={onX}>乘2</button>
        <button onClick={onMinus}>减一</button>
        <button onClick={onReset}>重置</button>
        <div>time: {timer}</div>
    </>);
}
export default UseStateFunction;