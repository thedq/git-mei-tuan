import React, { useState } from "react";
function UseStateFunction() {
    //2:timer初始值
    const [timer, setTimer] = useState(2);
    //setTimer接受的是一个事件对象
    return (<>
        <button onClick={() => { setTimer(timer * 2) }}>乘2</button>
        <div>time: {timer}</div>
    </>);
}
export default UseStateFunction;