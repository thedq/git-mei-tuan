import React, { useState, useEffect, useContext,useCallback } from "react";
export default function UseCallBack() {
    const [name, setName] = useState('DQ');
    useEffect(() => {
        setInterval(() => {
            //  setInterval防止闭包
            setName(oldName => oldName + '-XX');
        }, 1000);
    }, []);
    //使用useCallback(()缓存起来-》记忆单元格
    const sayHello = useCallback(() => {
        alert('我是父组件');
    });
    return (
        <div>
            <Children name={name} sayHello={sayHello} />
        </div>
    );
}
class Children extends React.PureComponent {
    render() {
        return (<div>{this.props.name}</div>);
    }
}