import React, { useState, useEffect, useContext, useCallback } from "react";
export default function UseCallBack() {
    const [name, setName] = useState('DQ');
    useEffect(() => {
        setInterval(() => {
            //  setInterval防止闭包
            setName(oldName => oldName + '-XX');
        }, 1000);
    }, []);
    const mm = 'mm';
    //使用useCallback(()缓存起来-》记忆单元格
    const sayHello = useCallback(() => {
        alert('我是父组件');
    });
    return (
        <div>
            <PureChildren name={mm} sayHello={sayHello} />
        </div>
    );
}
const Children = () => {
    console.log('子组件渲染');
    return (<div>静态组件</div>);
}
//使用React.memo包裹，memo比较static的变化
const PureChildren = React.memo(Children);
