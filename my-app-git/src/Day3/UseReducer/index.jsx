import React, { useState, useReducer } from 'react';
// 加减计数器
//useReducer函数
const reducer = (state, action) => {
    
    // 返回的是新的action
    if (action === '+') {
        return state + 1;
    } else if (action === '-') {
        return state - 1;
    }
}

//使用useReducer
export default function UseReducer() {
   const [count,dispatch] =useReducer(reducer,0);
    
    return (<div>
        <div>Count:{count}</div>
        <button onClick={() => dispatch('+')}>+</button>&nbsp;&nbsp;
        <button onClick={() => dispatch('-')}>-</button><br />
    </div>);
}
