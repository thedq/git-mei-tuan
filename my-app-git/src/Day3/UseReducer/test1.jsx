import React ,{useState}from 'react';
// 加减计数器
export default function UseReducer(){
    const[count, setCount]=useState(0);

    return (<div>
        <div>Count:{count}</div>
        <button onClick={()=>setCount(count+1)}>+</button>&nbsp;&nbsp;
        <button onClick={()=>setCount(count-1)}>-</button><br/>
    </div>);
}