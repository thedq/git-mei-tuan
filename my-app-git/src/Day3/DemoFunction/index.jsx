import React from "react";

function DemoFunction(props) {
    /*函数只能从上到下执行，没有render方法，组件没有渲染；只能使用Class去渲染*/
    return (
    <>
      <button onClick={props.x2}>x2</button>
      <div>第一个函数组件 {props.num}</div>
    </>
  );
}
class DemoFunctionClass extends React.Component {
     //使用state去实现定时器
  state = {
    num: 0,
  };
  timer;
  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({ num: this.state.num + 1 });
    }, 1000);
  }
  componentWillUnmount() {
       //卸载timer
    clearInterval(this.timer);
  }
  x2 = () => {
    this.setState({ num: this.state.num * 2 });
  };
  render() {
    const { num } = this.state;
     //对应的属性名称time
    return <DemoFunction num={num} x2={this.x2} />;
  }
}
export default DemoFunctionClass;
