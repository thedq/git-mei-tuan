import React, { useState, useEffect, useContext } from "react";
export default function TimerHook() {
    const count1 = useCounter(10);
    const count2 = useCounter(100);
    return (
        <div>
            TimerHook:<strong>{count1}</strong><br/>
            TimerHook:<strong>{count2}</strong><br/>
        </div>)
}

//自定义Timer
function useCounter(initialCount){
    const [count, setCount ]= useState(initialCount);
    useEffect(() => {
        const timer = setTimeout(() =>
            setCount(count + 1)
            , 1000);
        return () =>clearTimeout(timer);
    });
    return count;
}

function useCounter2(initialCount,dependencies){
    const [count, setCount ] = useState(initialCount);
    useEffect(() => {
        const timer = setTimeout(() =>
            setCount(count + 1)
            , 1000);
        return () =>clearTimeout(timer);
    },dependencies);
    return count;
}