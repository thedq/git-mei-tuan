import React, { useEffect, useState } from "react";
function UseEffectFunction() {
    console.log('函数被执行')
    const initialTimer = 10;
    const initialHello='dq'
    //2:timer初始值
    const [timer, setTimer] = useState(initialTimer);
    const[hello, setHello]=useState(initialHello);
    //setTimer接受的是一个事件对象
    const onX = () => {
        setTimer(timer * 2);
    }
    const onMinus = () => {
        setTimer(timer - 1);
    }
    const onReset = () => {
        setTimer(initialTimer);
    }
    // 定时器
    useEffect(() => {
        //延迟5秒再执行
       setTimeout(() => {
            setTimer(timer + 1);
            setHello(hello+"i");
        }, 1000);
        /*监听timer次数,每次timer变化的时候，执行第一个回调方法
        此时timer是最新的
        用到的变量都要监听
          */
         
    }, [timer,hello]);
    // 
  
    
    

    return (<>
        <button onClick={onX}>乘2</button>
        <button onClick={onMinus}>减一</button>
        <button onClick={onReset}>重置</button>
        <div>hello: {hello}</div>
        <div>time: {timer}</div>
        
    </>);
}
export default UseEffectFunction;