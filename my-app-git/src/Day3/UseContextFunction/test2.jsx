import React, { useContext, useEffect, useState } from "react";

// 创建Context
const UseContext = React.createContext();
//解构赋值
const { Provider, Consumer } = UseContext;
export default function useContextFunction() {
    const initName = 'Mike';
    const [name, setName] = useState(initName);
    //5000ms后修改name
    useEffect(() => {
        setTimeout(() =>setName('DQ'), 5000);
    },[])
    return (<>
        <Provider value={{
            name: name
        }}>
            {/* 任意的消费者 */}
            <Consumer>
                {value => <a>{value.name}</a>}
            </Consumer>
            <Children />
        </Provider>
    </>
    );
}

// 写法
function Children() {
    const value = useContext(UseContext);
    return (<>
        <h3>Children2</h3>
        <a>{value.name}</a>
    </>
    )
}