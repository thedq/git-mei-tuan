import React,{useContext} from "react";

// 创建Context
const UseContext = React.createContext();
console.log('Context', UseContext);
//解构赋值
const { Provider, Consumer } = UseContext;
export default function useContextFunction() {

    return (<>
        <Provider value={{
            name: 'Mike'
        }}>
            {/* 任意的消费者 */}
            <Consumer>
                {value => <a>{value.name}</a>}
            </Consumer>
            <Consumer>
                {value => <a>{value.name}</a>}
            </Consumer>
            <Children/>
            <Children2/>
        </Provider>
    </>
    );
}
// 写法1
function Children() {
    return (
        <>
            <h3>Children</h3>
            <Consumer>{value => <a>{value.name}</a>}</Consumer>
        </>);
}
// 写法2
function Children2(){
    const value=useContext(UseContext);
    return(<>
        <h3>Children2</h3>
        <a>{value.name}</a>
        </>
    )
}