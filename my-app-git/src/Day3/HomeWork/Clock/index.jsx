import React, { useState, useEffect, useContext } from 'react';
import './index.css';
//渲染时钟
const useTick = () => {
    //tick更新界面
    const [tick, setTick] = useState(0);
    useEffect(() => {
        const timer = setTimeout(() => setTick(tick + 1), 1000);
        //clearTimeout:下一次更新前和卸载组件时执行
        return () => { clearTimeout(timer); }
    }, [tick]);

};
//创建Context
const AlterContext = React.createContext();
const { Provider, Consumer } = AlterContext;
//通知时间
function AlarmTime() {
    // 这里的dateNow，可以使用
    const { time, message, dateNow } = useContext(AlterContext);
    useEffect(() => {
        if (time === String(dateNow)) {
            console.log('message:', message);
        }
    }, [dateNow]);
    return (<div className="alter-time">
        {/* div:块级元素，不需要br换行 */}
        <div>预设时间：{time}</div>
        <div>消息：{message}</div>
    </div>)
}

//设置指针
const setPoints = () => {
    const date = new Date();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    // 秒钟的角度
    const secondsDeg = (seconds / 60) * 360;
    // 分钟的角度要加上秒钟的角度；秒钟运行60度时，分钟也会变化--secondsDeg / 60
    const minutesDeg = (minutes / 60) * 360 + secondsDeg / 60;
    // 时钟的角度要加上分钟的角度
    const hoursDeg = (hours / 12) * 360 + minutesDeg / 12;
    //指针样式
    // 这里的deg是变量，POINTS应该小写，常量时才是大写
    const points = [
        { type: 'clock-point clock-point--hour', deg: hoursDeg },
        { type: 'clock-point clock-point--minute', deg: minutesDeg },
        { type: 'clock-point clock-point--second', deg: secondsDeg }];
    const dateNow = hours + ":" + minutes + ":" + seconds;
    const clock = { dateNow: dateNow, hours: hours, minutes: minutes, seconds: seconds };
    return { POINTS: points, clock };
}

function Clock() {
    //渲染时钟
    useTick();
    const { POINTS, clock } = setPoints();
    const time = '22:34:20';
    const message = '起床!';
    const dateNow = clock.dateNow;
    return (
        //   表
        <div className="clock">
            {/* 表盘 */}
            <div className="clock-bg">
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
            </div>
            <div>
                {/* 时、分、秒针 */}
                {POINTS.map((point, index) => {
                    //定义指针旋转的样式
                    const style = {
                        transform: `translate(-50%, 0) rotate(${point.deg}deg)`,
                    };
                    return (< div className={point.type} style={style} key={index} />);
                })
                }
                <div className="clock-dot"></div>
            </div>
            <div className="clock-time">
                {/* 当前时间 */}
                {/* 这里可以直接使用{dateNow} */}
                {clock.hours}:{clock.minutes}:{clock.seconds}
            </div>
            <Provider value={{ time: time, message: message, dateNow: dateNow }}>
                <AlarmTime />
            </Provider>
        </div>
    );
}
/*
时钟补0：
1)
let a='9';
a.length<2?'0'+a:a;
2)转换为number，和10作比较
3）lodash库：lodash.com/docs/4.17.15
_.padStart(9,2,'0')：前面补0：09
_.padEnd(9,2,'0')：后面补0：90
*/

export default Clock;
/*  
让所有的组件能获得时间，dateNow
1.方式1
export default ()=><Provider><Clock/></Provider>;

2.方式2
function WithClock=(Component)=>{
    return <Provider><Component/></Provider>
}
export default WithClock(Clock);
*/

// document.referrer:获得网站来源