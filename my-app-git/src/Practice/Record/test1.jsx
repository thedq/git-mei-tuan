import React from 'react';
export default class Practice extends React.Component {
    static defaultProps = {
        time: 4,
    };
    state = {
        time: 20
    };
    componentDidMount() {
        this.timer = setInterval(() => {
            //() => console.log('setState'):this.setState的回调函数
            this.setState((state, props) => ({ time: state.time + props.time }), () => console.log('setState'));
        }, 1000);
        this.forceUpdate();
    };
    componentWillUnmount() {
        clearInterval(this.timer);
    };
    render() {
        const { time } = this.state;
        return (<div>{time}</div>);
    };
}