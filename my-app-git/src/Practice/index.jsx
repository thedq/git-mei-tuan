import React from 'react';
export default class Practice extends React.Component {
    static defaultProps = {
        time: 4,
    };
    state = {
        time: 20
    };
    componentDidMount() {
        this.timer = setInterval((state, props) => {
            this.setState((state, props) => ({ time: props.time + state.time }), () => console.log(""));
        }, 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timer);
    };
    render() {
        const { time } = this.state;
        return (<div>{time}</div>);
    };
}