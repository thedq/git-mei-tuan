import React, { useEffect, useState } from "react";
import axios from 'axios';
const GetUser = () => {
    const [user, setUser] = useState({});
    // 原始发起请求
    useEffect(() => {
        axios.get('https://xiaozhu.run/api/user/9527').then((resp) => {
            const data = resp.data;
            if (data.code !== 200) {
                throw '服务器开小差了!';
            }
            return data.data;
        }).then(user => setUser(user)).catch(resp => { throw resp; })
    }, []);
    return (<div>
        <div>Get User:9527</div>
        <div>Email:{user.email}</div>
        <div>Name:{user.username}</div>
    </div>
    )
}
export default GetUser;