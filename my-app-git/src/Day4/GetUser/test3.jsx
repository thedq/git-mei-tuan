import React, { useState, useEffect } from "react";
import axios from "axios";

// 2. async
const request = async (url) => {
  const res = await axios.get(url);
  const data = res.data;
  if (data.code !== 200) {
    alert("服务器开小差了");
    return {};
  }
  return data.data;
};

const GetUser = () => {
  const [user, setUser] = useState({});
  const [id,setId] = useState(9527)
  const url = `https://xiaozhu.run/api/user/${id}`;

  const getUser = async () => {
    const user = await request(url);
    setUser(user);
  };

    useEffect(() =>getUser(), [id]);
    return (
      <div>
        <button onClick={()=>{setId(id+1);}}>Get User {id}</button>
        <div>Email: {user.email}</div>
        <div>Name: {user.username}</div>
      </div>
    );
  };
export default GetUser;
