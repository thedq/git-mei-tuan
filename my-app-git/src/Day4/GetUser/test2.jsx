import React, { useState, useEffect } from "react";
import axios from "axios";

// 1.
// const request = (url) => {
//     axios.get(url).then((res) => {
//       const data = res.data;
//       if (data.code !== 200) {
//         alert("服务器开小差了");
//         return {};
//       }
//       return data.data;
//     });
// };

// 2. async
const request = async (url) => {
  const res = await axios.get(url);
  const data = res.data;
  if (data.code !== 200) {
    alert("服务器开小差了");
    return {};
  }
  return data.data;
};

const GetUser = () => {
  const [user, setUser] = useState({});
  const url = "https://xiaozhu.run/api/user/9527";
  useEffect(() => {
    request(url).then((user) => setUser(user));
  }, []);
  return (
    <>
      <button>Get User {9527}</button>
      <div>Email: {user.email}</div>
      <div>Name: {user.username}</div>
    </>
  );
};
export default GetUser;
