import React, { useRef, useState, useEffect, useReducer } from "react";
// 使用Hook封装
const useForm = initialFides=> {
    const formRenderer = (state, action) => {
        console.log('执行formRenderer', action);
        const { key, value } = action;
        return { ...state, [key]: value };
    };

    const [values, dispatch] = useReducer(formRenderer, initialFides);
    const getFieldProps = key=> ({
        value: values[key],
        onChange: e => dispatch({ key, value: e.target.value })
    });

    return [values, getFieldProps];
}
export default function Input() {
    // {}设置初始值
    const [values, getFieldProps] = useForm({ user: '', pass: '' });
    return (
        <div>
            <input value={values.user} {...getFieldProps('user')} />
            <input type="password" placeholder="password"  {...getFieldProps('pass')} />
            <button>提交</button>
        </div>
    )
}
