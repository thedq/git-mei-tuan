import React, { useRef, useState, useEffect, useReducer } from "react";
// 使用Hook封装
const useForm=(initialFides)=>{
    const formRenderer = (state,action) => {
        console.log('执行formRenderer',action);
        return {...state, ...action};
    };
    return useReducer(formRenderer,initialFides);
}
export default function Input() {
    // {}设置初始值
    const [values, dispatch] = useForm( { user: '', pass: '' });
    return (
        <div>
            <input value={values.user} onChange={e => dispatch({user:e.target.value})} />
            <input type="password" placeholder="password" value={values.pass} onChange={e => dispatch({pass:e.target.value})} />
            <button>提交</button>
        </div>
    )
}
