import React, { useRef,useState } from "react";
export default function Input() {
const[value,setValue] =useState('默认值');
const [pass,setPass]=useState('PASS');
    return (
        <div>
            {/* 每次输入新的值，就会重新渲染 */}
            <input value={value} onChange={e=>setValue(e.target.value)}/>   
            <input type="password" placeholder="password" value={pass} onChange={e=>setPass(e.target.value)}/>   
            <button>提交</button>  
        </div>
    )
}