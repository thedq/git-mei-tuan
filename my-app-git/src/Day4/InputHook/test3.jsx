import React, { useRef,useState} from 'react';
// 封装Input
function UseInput(props){
    const[values,setValues] =useState('默认值');
    return (
        <div>
            <input value={values.value} onChange={e=>{props.onInputChange(e.target.value);setValues(e.target.value)} }/>   
            
        </div>
    )
}
// 函数设置默认props
UseInput.defaultProps={
    onInputChange:()=>{

    }
}