import React, { useRef } from "react";
export default function Input() {
    const ref = useRef();
    //Object,ref是一个容器
    //console.log(ref);
    return (
        <div>
            <input ref={ref} />
            <button onClick={() => console.log(ref)} >Get Ref</button>
        </div>
    )
}