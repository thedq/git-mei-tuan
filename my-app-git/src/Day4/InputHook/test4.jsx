import React, { useRef,useState } from "react";
export default function Input() {
const[values,setValues] =useState({user:'',pass:''});
console.log(values);
    return (
        <div>
            {/* 每次输入新的值，就会重新渲染 */}
            {/* 
            ...values:解构赋值
            等同于：
            const newValues={...values};
            newValues.user=e.target.value;
             */}
            <input value={values.user} onChange={e=>setValues({...values,user:e.target.value})}/>   
            <input type="password" placeholder="password" value={values.pass} onChange={e=>setValues({...values,pass:e.target.value})}/>   
            <button>提交</button>  
        </div>
    )
}
