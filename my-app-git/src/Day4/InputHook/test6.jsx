import React, { useRef, useState, useEffect, useReducer } from "react";
// formRenderer第一次进来就会执行，需要返回值，否则就会是undefined
const formRenderer = (state,action) => {
    console.log('执行formRenderer',action);
    return {...state, ...action};
};
export default function Input() {
    // {}设置初始值
    const [values, dispatch] = useReducer(formRenderer, { user: '', pass: '' });
    return (
        <div>
            <input value={values.user} onChange={e => dispatch({user:e.target.value})} />
            <input type="password" placeholder="password" value={values.pass} onChange={e => dispatch({pass:e.target.value})} />
            <button>提交</button>
        </div>
    )
}
