import React, { useRef, useState, useEffect } from "react";
export default function Input() {
    const [values, setValues] = useState({ user: '', pass: '' });
    useEffect(() => {
        const timer = setTimeout(() => console.log(values), 5000);
        return () => clearTimeout(timer);
    }, [])
    const setFields = (key, value) => {
        /* 用于将所有可枚举属性的值从一个或多个源对象分配到目标对象。它将返回目标对象。 
         const newValues=Object.assign({},values,{[key]:value});
        */
        setValues({ ...values, [key]: value });
    };
    return (
        <div>
            <input value={values.user} onChange={e => setFields('user', e.target.value)} />
            <input type="password" placeholder="password" value={values.pass} onChange={e => setFields('pass', e.target.value)} />
            <button>提交</button>
        </div>
    )
}
