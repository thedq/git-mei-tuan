import React from "react";
import './index.css';

class NumberListButton extends React.Component {
    state = {
        color: 0
    }


    // 奇数
    setOdd = () => {
        this.setState({
            color: 1
        })
    }
    //偶数
    setEven = () => {
        this.setState({
            color: 2
        })
    }
    //正常展示
    setShow = () => {
        this.setState({
            color: 3
        })
    }
    render() {
        const list = [2, 4, 6, 8, 10];
        const { color } = this.state;
        return (
            <div>
                <div>
                    <button onClick={this.setOdd}>奇数行加深</button>
                    <button onClick={this.setEven}>偶数行加深</button>
                    <button onClick={this.setShow}>正常展示</button><br />
                </div>
                <div>
                    {
                        list.map((item, index) => {

                            const isEven = !!((index) % 2);
                            let cls = '';
                            //奇数
                            if (color === 1) {
                                cls = `line line${isEven ? '--shallow' : '--deep'}`;
                                //偶数
                            } else if (color === 2) {
                                cls = `line line${isEven ? '--deep' : '--shallow'}`;
                                //正常
                            } else if (color === 3) {
                                cls = `line line--shallow`;
                            }
                            return <p key={item} className={cls}>{item}</p>
                        })
                    }
                </div>
            </div>);
    }
}
export default NumberListButton;