import React from "react";
const STATUS = {
    even: 1,
    odd: 2,
    normal: 0,
  };
  export default class NumberListButton extends React.Component {
    state = {
      status: STATUS.normal,
    };
    render() {
      const { status } = this.state;
      const list = [2, 4, 6, 8, 10];
      const btns = [
        { label: "奇数行加深", status: STATUS.even },
        { label: "偶数行加深", status: STATUS.odd },
        { label: "正常展示", status: STATUS.normal },
      ];
      const items = list.map((item, index) => {
        const isEvent = !(index % 2);
        let isActive = false;
        if (
          (status === STATUS.even && isEvent) ||
          (status === STATUS.odd && !isEvent)
        ) {
          isActive = true;
        }
        const cls = `line line${isActive ? "--deep" : "--shallow"}`;
        return (
          <div className={cls} key={index}>
            {item}
          </div>
        );
      });
      return (
        <>
          {btns.map((btn) => {
            const { status: btnStatus, label } = btn;
            return (
              <button
                key={btnStatus}
                style={{ color: status === btnStatus ? "red" : "" }}
                onClick={() => this.onStatusChange(btnStatus)}
              >
                {label}
              </button>
            );
          })}
          <div className="items">{items}</div>
        </>
      );
    }
    onStatusChange = (status) => {
      this.setState({ status });
    };
  }
 
  