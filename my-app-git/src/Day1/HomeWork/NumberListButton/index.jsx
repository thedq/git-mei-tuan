import React from "react";
import './newIndex.css';
const STATUS = {
  odd: 1,//奇数
  even: 2,//偶数
  normal: 0,//正常显示
};
export default class NumberListButton extends React.Component {
  state = {
    status: STATUS.normal,//正常显示
  };
  render() {
    const { status } = this.state;
    const list = [2, 4, 6, 8, 10];
    const btns = [
      { label: "奇数行加深", status: STATUS.odd },
      { label: "偶数行加深", status: STATUS.even },
      { label: "正常展示", status: STATUS.normal },
    ];
    const items = list.map((item, index) => {
      // 取反，变为true,false
      const isEvent = !(index % 2);// 0%2===0 取反：true
      let isActive = false;//是否被选中，奇偶都是被选中
      //console.log(status,isEvent)//奇数 1 true
      if (
        (status === STATUS.odd && isEvent) ||
        (status === STATUS.even && !isEvent)
      ) {
        isActive = true;
      }
      /*
      奇数行
      line line--active
      line 
      line line--active
      line 
      line line--active

       偶数行：
      line 
      line line--active
      line 
      line line--active
      line 
       */
      const cls = `line ${isActive ? 'line--active' : ''}`;
      //console.log(cls);
      return (
        <div className={cls} key={index}>
          {item}
        </div>
      );
    });
    return (
      <>
        {btns.map((btn) => {
          const { status: btnStatus, label } = btn;
          return (
            <button
              key={btnStatus}
              // 状态有无改变，无改变则为被选中
              style={{ color: status === btnStatus ? "red" : "" }}
              onClick={() => this.onStatusChange(btnStatus)}
            >
              {label}
            </button>
          );
        })}
        <div className="items">{items}</div>
      </>
    );
  }
  onStatusChange = (status) => {
    this.setState({ status });
  };
}

