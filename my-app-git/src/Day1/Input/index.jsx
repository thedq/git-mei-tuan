import React from "react";
export default class Input extends React.Component {
    inputRef = React.createRef();
    state = {
        value: ''
    }

    onInputFocus = () => {
        this.setState({
            value: '默认的初始值'
        });
    }
    onInputChange = event => {
        console.log(event.target.value);
        /* 
        event.target.value
        event.target.value*2
        event.target.value+event.target.value
         */
        this.setState({value: event.target.value});
    }
    render() {
        const { value } = this.state;

        return (
            <>
            <h1>{value}</h1>
                <input
                    onFocus={this.onInputFocus}
                    onChange={this.onInputChange}
                    value={value}
                />
            </>
        );
    }


}
