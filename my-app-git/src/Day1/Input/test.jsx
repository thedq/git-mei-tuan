import React from "react";
export default class Input extends React.Component {
    inputRef = React.createRef();
    state = {
        greeting: 'dq'
    }
    sayHello = () => {
        alert("Hello " + this.state.greeting);
    };
    // 输入框高亮，提示用户
    componentDidMount() {
        this.inputRef.current.focus();
        this.inputRef.current.value = "";
        this.inputRef.current.sayHello();
    }

    render() {
        return <input ref={this.inputRef}></input>;
    }
}
