/* 函数类型组件 */
import React from "react";

class HelloWorldProps extends React.Component {
    // 设置默认属性
    static defaultProps = {
        title: 'dq',
        content: '默认文本还没有内容',
        a: 'a'

    }
    // 组件的state
    state = { times: 0, focus: 3, mood: 100 };
    //react生命周期，页面渲染完后加载
    componentDidMount() {
        //循环定时器，每隔1000ms执行
       this.timer= setInterval(() => {
            // const { mood } = this.state;
            // this.setState({ mood: this.state.mood + 100 });
            this.setState({
                times: this.state.times + 1,
                focus: this.state.focus + 10,
            });
            // 强制更新
            this.forceUpdate();
        }, 1000);//1000ms
    }
    componentWillUnmount(){
        clearInterval(this.timer);
    }

    render() {
        console.log(this.props);
        return <div>
            {/* this.props.xxx读取属性的值 */}
            {this.props.title}<br />
            {this.props.content}<br />
            {this.props.a}<br />
            {this.props.b}<br />
            <h2>{this.props.title}--{this.state.times}</h2>
            <h3>{this.state.focus}</h3>
            <p>{this.state.mood}</p>
        </div>;
    }

}
export default HelloWorldProps;
