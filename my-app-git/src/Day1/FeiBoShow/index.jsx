import React from "react";
class FeiBoShow extends React.Component {
    state = { list: [0, 1] };
    componentDidMount() {
        // 内存泄漏
      this.timer=  setInterval(() => {
            //取数组末尾两个元素
            const [a, b] = this.state.list.slice(-2);
            this.setState({
                /*
                ...解构赋值:把list数组的结构和值赋值
                在数组末尾追加a+b
                */
                list: [...this.state.list, a + b]
            });
        }, 1000);
        
    }
    //卸载循环定时器
    componentWillUnmount(){
        clearInterval(this.timer);
    }

    render() {
        return (
            <>
                {this.state.list.map((item, index) => (
                    <span key={index} >{item}  </span>
                ))}
            </>
        );
    }
}

export default FeiBoShow;