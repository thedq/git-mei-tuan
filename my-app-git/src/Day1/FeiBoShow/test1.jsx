import React from "react";
class FeiBoShow extends React.Component {
    state = { list: [0, 1] };
    componentDidMount() {
        this.timer=setInterval(() => {
             //取数组末尾两个元素
            const [a, b] = this.state.list.slice(-2);
            const c = a + b;

            //...解构赋值:把list数组的结构和值赋值给newList
            const newList = [...this.state.list];
            //在数组末尾追加c
            newList.push(c);
            
            this.setState({
                list: newList,
            });
        }, 1000);
    }
    componentWillUnmount(){
        clearInterval(this.timer);
    }

    render() {
        return (
            <>
                {this.state.list.map((item, index) => (
                    <span key={index} >{item}  </span>
                ))}
            </>
        );
    }
}

export default FeiBoShow;