import React from "react";
class FeiBoShow extends React.Component {
    state = { list: [0] };
    componentDidMount() {
        this.timer = setInterval(() => {
            const num = this.add(this.state.list.length - 1);
            this.setState({
                list: [...this.state.list, num],
            });
        }, 1000);
    }
    add(num) {
        if (num === 1 || num === 2 || num === 0) {
            return 1;
        }
        return this.add(num - 1) + this.add(num - 2);
    }
    render() {
        return (
            <div>
                {this.state.list.join(',')}
            </div>
        );
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }
}

export default FeiBoShow;