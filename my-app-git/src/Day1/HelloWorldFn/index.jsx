/* 函数类型组件 */
import React from "react";
//3.导入css import './index.css';
function HelloWorldFn(){
    function handleClick(){
        console.log('Hello World');
        alert('function HelloWorldFn');
    }
    // 1.外部变量引用
    const style={color: 'green'};
    /* 
    2.css直接写
    {}外面的--变量
    {}里面声明属性
    style={{color: 'red'}}
     */
    return <div style={style} onClick={handleClick}>Hello World</div>;
}
export default HelloWorldFn;
