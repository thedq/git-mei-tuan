import React from "react";
//创建Context
const { Provider, Consumer } = React.createContext();
export default class Context extends React.Component {

    render() {
        return (
            // 提供Provider
            <Provider value={{ title: 'Hello DQ' }}>
                {/* 通过Consumer将值传递下去 */}
                <Demo />
            </Provider>
        )
    }
}

class Demo extends React.Component {
    render() {
        return (
            <>
                {
                    <Consumer>{value => <h3>{value.title}</h3>}</Consumer>
                }
            </>
        )
    }
}

