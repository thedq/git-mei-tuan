import React from "react";
// 导入css样式
import './index.css';
class HelloWorld extends React.Component {
    handleClick = () => {
        alert("HelloWorld");
    }
    setDataClick = () => {
        alert("test");
    }
    // 渲染
    render() {
        // 常量
        const text="HelloWorld";
        /* 只会返回一个标签，包裹起来 */
        return <div>
            <h1 onClick={this.setDataClick}>test</h1>
            <div  onClick={this.handleClick}>HelloWorld</div>
            {/* 类名选择器命名：className='hello-world'，避免和class关键字混淆 */}
            <div className="hello-world" >{text}</div>
        </div>;
    }

}
// 默认暴露
export default HelloWorld;
