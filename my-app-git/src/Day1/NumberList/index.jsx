import React from "react";
import './index.css';
/* 让偶数行背景色不一样 */
class NumberList extends React.Component {
    // 渲染
    render() {
        const list = [2, 4, 6, 8, 10];
        return (
            <div>
                {
                    list.map((item, index) => {
                        /* 
                        更规范化
                        ！:取非转换为boolean【true,false】
                        !:取非，再转换为数字【1 0 】
                        不用！！也行
                         */
                        const isEven = !!(index % 2);
                        /* 模板字符串：line-even或line-no */
                        const cls = `line ${isEven ? 'line--even' : ''}`;
                        // key：键值 
                        return <p key={item} className={cls}>{item}</p>
                    })
                }
            </div>
        );
    }

}
// 默认暴露
export default NumberList;
