import "./App.css";
import React from "react";
// Day1
import HelloWorld from "./Day1/HelloWorld";
import HelloWorldFn from "./Day1/HelloWorldFn";
import FeiBoShow from "./Day1/FeiBoShow";
import Input from "./Day1/Input";
import NumberList from "./Day1/NumberList";
import NumberListFn from "./Day1/NumberListFn";
import HelloWorldProps from "./Day1/HelloWorldProps";
import NumberListButton from "./Day1/HomeWork/NumberListButton";
import Practice from "./Practice";
import Context from "./Day1/Context";
//Day2
import Hello from "./Day2/Hello";
import Constructor from "./Day2/Constructor";
import FetchUrl from "./Day2/FetchUrl";
import FetchUsers from "./Day2/FetchUsers";
import FetchUrlFromId from "./Day2/FetchUlFromId";
import Form from "./Day2/Form";
import FormHoc from "./Day2/FormHoc";
import windowWidthHoc from "./Day2/WindowWidth";
import Calculator from "./Day2/HomeWork/Calculator";
//Day3
import DemoFunctionClass from "./Day3/DemoFunction";
import UseStateFunction from "./Day3/UseStateFunction";
import UseEffectFunction from "./Day3/UseEffectFunction";
import UseContextFunction from "./Day3/UseContextFunction";
import TimerHook from "./Day3/TimerHook";
import UseReducer from "./Day3/UseReducer";
import UseCallBack from "./Day3/UseCallBack";
import UseMemo from "./Day3/UseMemo";
import Clock from "./Day3/HomeWork/Clock";
//Day4
import InputHoc from "./Day4/InputHook";
import GetUser from "./Day4/GetUser";
//Order项目
import Order from "./Order";
const menus = [
  // title标题 Component组件 groupTitle分类
  // Day1
  { title: "Hello World", Component: HelloWorld, groupTitle: "Day1" },
  {
    title: "Hello World Function",
    Component: HelloWorldFn,
  },
  { title: "Number List", Component: NumberList },
  {
    title: "Number List Function",
    Component: NumberListFn,
  },
  { title: "FeiBoShow", Component: FeiBoShow },
  {
    title: "Hello World Props",
    Component: HelloWorldProps,
  },
  {
    title: "Context",
    Component: Context,
  },
  { title: "Input", Component: Input },
  {
    title: "Number List Button",
    Component: NumberListButton,
    groupTitle: "Day1 HomeWork",
  },
  // Day2
  { title: "Hello", Component: Hello, groupTitle: "Day2" },
  { title: "Constructor", Component: Constructor },
  { title: "Fetch Url", Component: FetchUrl },
  { title: "Fetch Users", Component: FetchUsers },
  { title: "Fetch Url From Id", Component: FetchUrlFromId },

  { title: "Form", Component: windowWidthHoc(Form) },
  {
    title: "Form Hoc",
    Component: () => (
      <FormHoc title="HHHHH" btnText="用户登录2" defaultUsername="Hoc 初始值" />
    ),
  },
  { title: "Window Width Hoc", Component: windowWidthHoc(Form) }, // 高阶组件包裹
  { title: "Calculator", Component: Calculator, groupTitle: "Day2 HomeWork" },
  //Day3
  {
    title: "Demo Function Class",
    Component: DemoFunctionClass,
    groupTitle: "Day3",
  },
  { title: "UseState Function", Component: UseStateFunction },
  { title: "UseEffect Function", Component: UseEffectFunction },
  { title: "UseContext Function", Component: UseContextFunction },
  { title: "Timer Hook", Component: TimerHook },
  { title: "UseReducer", Component: UseReducer },
  { title: "UseCallBack", Component: UseCallBack },
  { title: "UseMemo", Component: UseMemo },
  { title: "Clock", Component: Clock, groupTitle: "Day3 HomeWork" },
  //Day4
  { title: "Input Hoc", Component: InputHoc, groupTitle: "Day4" },
  { title: "Get User", Component: GetUser},
  //Order项目
  { title: "Order", Component: Order,groupTitle: "Order Project"},
  //Practice
  { title: "Practice", Component: Practice, groupTitle: " Practice" },
];

class App extends React.Component {
  constructor(props) {
    super(props);
    // 当前所选文件下标
    this.state = {
      menuIndex: 0,
    };
  }
  componentDidMount() {
    console.log("window.location.hash", window.location.hash);
    //字符串转换为Number
    //substring(1)：去掉#
    const menuIndex = Number(window.location.hash.substring(1));
    this.setState({
      menuIndex,
    });
  }
  onMenuChange = (menuIndex) => {
    this.setState({ menuIndex });
    // 当前被选中文件的下标设置为哈希值
    window.location.hash = menuIndex;
  };

  render() {
    const { menuIndex } = this.state;
    // const { title, Component } = menus[menuIndex];
    const { Component } = menus[menuIndex];
    return (
      <div className="app">
        <div className="menu">
          <ul>
            {menus.map((menu, index) => {
              //数据类型一样
              const isCurrent = index === menuIndex;
              const groupTitle = menu.groupTitle;
              return (
                <li
                  className={isCurrent ? "active" : ""}
                  onClick={() => this.onMenuChange(index)}
                  key={index}
                >
                  {groupTitle && <h2>{groupTitle}</h2>}
                  {menu.title}
                </li>
              );
            })}
          </ul>
        </div>
        <div className="main">
          <Component />
        </div>
      </div>
    );
  }
}

export default App;
