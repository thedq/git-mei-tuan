import React from "react";
import Form from '../Form';
//import FetchUrl from '../FetchUrl';
function formHoc(Component) {
    class WrappedComponent extends React.Component {
       static defaultProps = {
           defaultUsername:'新的初始值'
       }
       onSayHello =()=>{
           this.alter('hello');
       }
        render() {
            const newProps = { ...this.props, defaultUsername: '新的初始值' };
            return <>
                <h2>用户表单</h2>
                <Component {...this.props} xxx={123} defaultUsername='绝对优先级' />
                {/* <Component {...newProps} /> */}
            </>
        }
    }
    return WrappedComponent;

}
const NewForm = formHoc(Form);
//const NewFetchUrl=formHoc(FetchUrl);
export default NewForm;
//export default NewFetchUrl;