import React from 'react';
import Form from '../Form';

function formHoc(Component) {
  class WrappedComponent extends React.Component {
    static defaultProps = {
      defaultUsername: '新的初始值',
    };

    sayHello = () => {
      alert('hello');
    };

    render() {
      const { title, ...restProps } = this.props;
      return (
        <>
          <h2>{title}</h2>
          <Component
            {...restProps}
            sayHello={this.sayHello}
            defaultUsername="绝对优先级"
          />
        </>
      );
    }
  }
  return WrappedComponent;
}

const FormHoc = formHoc(Form);

export default FormHoc;
