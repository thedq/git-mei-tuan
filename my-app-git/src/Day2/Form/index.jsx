import React from 'react';

// @formHoc
export default class Form extends React.Component {
    static defaultProps = {
        defaultUsername: 'anonymous',
        btnText: '登录',
    };

    state = {
        username: this.props.defaultUsername,
    };
    //e:合成事件
    onUsernameChange = e => {
        console.log(e.target.value);
        this.setState({ username: e.target.value });
        this.props.sayHello && this.props.sayHello();
    };

    render() {
        const { username } = this.state;
        const { btnText, windowWidth } = this.props;
        return (
            <form>
                <input
                    type="text"
                    onChange={this.onUsernameChange}
                    value={username}
                    placeholder="用户名"
                />
                <input type="submit" value={btnText} />
                窗口宽度：{windowWidth}
            </form>
        );
    }
}
