/* teacher版本二 */
import React, { Component } from "react";
import "./index.css";
// 加减乘除
const EXP = {
  plus: 0,
  minus: 1,
  multiply: 2,
  divide: 3,
};
// 控制按钮
const CTRL = {
  reset: 0,//清空
  revert: 1,//取反
  percent: 2,//%百分号
};
//定义按钮的属性
// label:按钮的名称 type:按钮类型 value:点击按钮之后输入的值
const BTNS = [
  { label: "C", type: "CTRL", value: CTRL.reset },
  { label: "+/-", type: "CTRL", value: CTRL.revert },
  { label: "%", type: "CTRL", value: CTRL.percent },
  { label: "÷", type: "EXP", value: EXP.divide },
  { label: "7", type: "NUMBER", value: "7" },
  { label: "8", type: "NUMBER", value: "8" },
  { label: "9", type: "NUMBER", value: "9" },
  { label: "x", type: "EXP", value: EXP.multiply },
  { label: "4", type: "NUMBER", value: "4" },
  { label: "5", type: "NUMBER", value: "5" },
  { label: "6", type: "NUMBER", value: "6" },
  { label: "-", type: "EXP", value: EXP.minus },
  { label: "1", type: "NUMBER", value: "1" },
  { label: "2", type: "NUMBER", value: "2" },
  { label: "3", type: "NUMBER", value: "3" },
  { label: "+", type: "EXP", value: EXP.plus },
  { label: "0", type: "NUMBER", value: "0" },
  { label: ".", type: "NUMBER", value: "." },
  { label: "=", type: "CALC", value: null },
];
export default class Calculator extends Component {
  state = {
    number1: "0",
    exp: null,
    number2: "0",
  };
  onNumberClick = (value) => {
    const { exp, number1, number2 } = this.state;
    let preNumber = exp === null ? number1 : number2;
    if (/\./.test(preNumber) && value === ".") {
      return;
    }
    let newNumber = preNumber + value;
    if (!/^0\./.test(number1)) {
      newNumber = newNumber.replace(/^0/, "");
    }
    if (exp === null) {
      this.setState({ number1: newNumber });
    } else {
      this.setState({ number2: newNumber });
    }
  };
  onCtrlClick = (ctrl) => {
    const { number1, exp, number2 } = this.state;
    let preNumber = exp === null ? number1 : number2;
    switch (ctrl) {
      case CTRL.reset:
        this.setState({ number1: 0, exp: null, number2: 0 });
        break;
      case CTRL.revert:
        preNumber = String(-Number(preNumber));
        if (exp === null) {
          this.setState({ number1: preNumber });
        } else {
          this.setState({ number2: preNumber });
        }
        break;
      case CTRL.percent:
        preNumber = String(Number(preNumber) / 100);
        if (exp === null) {
          this.setState({ number1: preNumber });
        } else {
          this.setState({ number2: preNumber });
        }
        break;
      default:
        break;
    }
  };
  onExpClick = (exp) => {
    this.setState({ exp });
  };
  onCalc = () => {
    let { number1, exp, number2 } = this.state;
    let newNumber;
    if (typeof number1 === "undefined") {
      this.setState({ number1: "0" });
      return;
    }
    number1 = Number(number1);
    number2 = Number(number2);
    switch (exp) {
      case EXP.plus:
        newNumber = String(number1 + number2);
        break;
      case EXP.minus:
        newNumber = String(number1 - number2);
        break;
      case EXP.multiply:
        newNumber = String(number1 * number2);
        break;
      case EXP.divide:
        newNumber = String(number1 / number2);
        break;
      default:
        newNumber = number1;
        break;
    }
    this.setState({ number1: newNumber, exp: null, number2: 0 });
  };
  render() {
    console.log(this.state);
    let { number1, exp, number2 } = this.state;
    //判断是否用户输入的数字过长--number1和number2
    number1 = number1.length >= 10 ? "数字过长" : number1;
    number2 = number2.length >= 10 ? "数字过长" : number2;
    let currNumber = exp === null ? number1 : number2;
    return (
      <>
        <h2>计算器</h2>
        <div className="calc">
          {/* 屏幕 */}
          <div className="calc-screen">{currNumber}</div>
          {/*  按钮*/}
          {BTNS.map((btn, index) => {
            //onClick是一个对象，EXP、CTRL、NUMBER、CALC键名，
            //通过{}[btn.type]获取对应的按钮点击事件
            const onClick = {
              EXP: () => {
                this.onExpClick(btn.value);
              },
              CTRL: () => {
                this.onCtrlClick(btn.value);
              },
              NUMBER: () => {
                this.onNumberClick(btn.value);
              },
              CALC: this.onCalc,
            }[btn.type];
            const cls = {
              EXP: "exp",
              CTRL: "ctrl",
              // 如果是0按钮，返回zero，调用.calc button.zero的css样式
              NUMBER: btn.value === "0" ? "zero" : "",
              CALC: "exp",
            }[btn.type];
            return (
              <button className={cls} onClick={onClick} key={index}>
                {btn.label}
              </button>
            );
          })}
        </div>
      </>
    );
  }
}
