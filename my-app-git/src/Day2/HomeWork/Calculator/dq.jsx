import React from 'react';
import './index.css';
// 加减乘除
const EXP = {
  plus: 0,
  minus: 1,
  multiply: 2,
  divide: 3,
};
//控制运算
const CTRL = {
  reset: 0,//清空C
  revert: 1,//+/-:取反，正数<->负数
  percent: 2,//%
};
// 是否按下了等号
let isResActive = false;

export default class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // 上一个数
      number1: '0',
      // 运算符：加减乘除
      exp: null,
      //第二个数
      number2: '0',
    };
  }


  //数字检查事件
  onPreNumberCheck = () => {
    const { number1, exp, number2 } = this.state;
    if (exp !== null) {
      return number2;
    }
    return number1;

  }
  // 长度检查事件：超过10位后，提示错误
  onLengthCheck = (currentNumber) => {
    if (String(currentNumber).length > 10) {
      return 'NaN';
    }
    return currentNumber;
  }

  //运算符检查事件
  onExpCheck = (prevNumber) => {
    const { exp, number1, number2 } = this.state;
    if (exp === null) {
      this.setState({ number1: prevNumber });
    } else {
      this.setState({ number2: prevNumber });
    }
  }
  //清空事件
  onReset = () => {
    this.setState({ number1: 0, number2: 0, exp: null });
  }
  //数字按钮点击事件
  onNumberClick = value => {
    const { exp, number1, number2 } = this.state;
    let preNumber = this.onPreNumberCheck();
    // 当前面是小数点时，再次点击小数点则直接返回
    if (/\./.test(preNumber) && value === '.') {
      return;
    }
    let newNumber = preNumber + value;

    //去掉非0.和0的数：!/^0\.|0/
    //排除如00的情况--匹配以0开头的数字，但不是小数：^0\d+
    if (!/^0\.|0/.test(number1) || (/^0\d+/.test(newNumber))) {
      // 删掉第一位0
      newNumber = newNumber.replace(/^0/, '');
    }
    //如果输入的数超过10位
    if (this.onLengthCheck(number1) === 'NaN') {
      //错误提示
      newNumber = 'NaN';
      //清空
      this.onReset();
    }
    //按下等号展示结果，再按下数字，结果栏会被重置
    //排除00的情况
    if ((isResActive && number1 !== '0' && number2 === '0' && exp === null)) {
      newNumber = '0';
      this.onReset();
    }
    isResActive = false;
    this.onExpCheck(newNumber);
  };

  // 控制按钮点击事件
  onCtrlClick = ctrl => {
    let prevNumber = this.onPreNumberCheck();
    isResActive = false;
    switch (ctrl) {
      case CTRL.reset:
        this.onReset();
        break;
      case CTRL.revert:
        prevNumber = String(-Number(prevNumber));
        this.onExpCheck(prevNumber);
        break;
      case CTRL.percent:
        prevNumber = String(Number(prevNumber) / 100);
        this.onExpCheck(prevNumber);
        break;
      default:
        this.onReset();
        break;
    }
  };

  //运算符按钮点击事件
  onExpClick = exp => {
    isResActive = false;
    this.setState({ exp });
  };
  //结果计算事件
  onCalc = () => {
    let { exp, number1, number2 } = this.state;
    isResActive = true;
    switch (exp) {
      case EXP.plus:
        number1 = String(Number(number1) + Number(number2));
        break;
      case EXP.minus:
        number1 = String(Number(number1) - Number(number2));
        break;
      case EXP.multiply:
        number1 = String(Number(number1) * Number(number2));
        break;
      case EXP.divide:
        number1 = String(Number(number1) / Number(number2));
        break;
      default:
        this.onReset();
        break;
    }
    this.setState({ number1, number2: '0', exp: null });
  };
  //渲染
  render() {
    console.log(this.state.number1, this.state.exp, this.state.number2);
    const currentNumber = this.onPreNumberCheck();
    /* 
    ctrl:C +/- % 
    exp:+ - * ÷ =
    */
    return (
      <div className="calc">
        <div className="calc-screen">{currentNumber}</div>
        <button onClick={() => this.onCtrlClick(CTRL.reset)} className="ctrl">
          C
        </button>
        <button onClick={() => this.onCtrlClick(CTRL.revert)} className="ctrl">
          +/-
        </button>
        <button onClick={() => this.onCtrlClick(CTRL.percent)} className="ctrl">
          %
        </button>
        <button onClick={() => this.onExpClick(EXP.divide)} className="exp">
          ÷
        </button>
        <button onClick={() => this.onNumberClick('7')}>7</button>
        <button onClick={() => this.onNumberClick('8')}>8</button>
        <button onClick={() => this.onNumberClick('9')}>9</button>
        <button onClick={() => this.onExpClick(EXP.multiply)} className="exp">
          x
        </button>
        <button onClick={() => this.onNumberClick('4')}>4</button>
        <button onClick={() => this.onNumberClick('5')}>5</button>
        <button onClick={() => this.onNumberClick('6')}>6</button>
        <button onClick={() => this.onExpClick(EXP.minus)} className="exp">
          -
        </button>
        <button onClick={() => this.onNumberClick('1')}>1</button>
        <button onClick={() => this.onNumberClick('2')}>2</button>
        <button onClick={() => this.onNumberClick('3')}>3</button>
        <button onClick={() => this.onExpClick(EXP.plus)} className="exp">
          +
        </button>
        <button onClick={() => this.onNumberClick('0')} className="zero">
          0
        </button>
        <button onClick={() => this.onNumberClick('.')}>.</button>
        <button onClick={() => this.onCalc()} className="exp">
          =
        </button>
      </div>
    );
  }
}
