import React from 'react';
import './index.css';
// 计算器teacher版本一
const EXP = {
  plus: 0,
  minus: 1,
  multiply: 2,
  divide: 3,
};

const CTRL = {
  reset: 0,
  revert: 1,
  percent: 2,
};

export default class Calc extends React.Component {
  state = {
    number1: '0',
    exp: null,
    number2: '0',
  };

  onNumberClick = value => {
    const { exp, number1, number2 } = this.state;
    let prevNumber = exp === null ? number1 : number2;
    if (/\./.test(prevNumber) && value === '.') {
      return;
    }
    let newNumber = prevNumber + value;
    if (!/^0\./.test(number1)) {
      newNumber = newNumber.replace(/^0/, '');
    }
    if (exp === null) {
      this.setState({ number1: newNumber });
    } else {
      this.setState({ number2: newNumber });
    }
  };

  onCtrlClick = ctrl => {
    const { exp, number1, number2 } = this.state;
    let prevNumber = exp === null ? number1 : number2;

    switch (ctrl) {
      case CTRL.reset:
        this.setState({ number1: 0, number2: 0, exp: null });
        break;
      case CTRL.revert:
        prevNumber = String(-Number(prevNumber));
        if (exp === null) {
          this.setState({ number1: prevNumber });
        } else {
          this.setState({ number2: prevNumber });
        }
        break;
      case CTRL.percent:
        prevNumber = String(Number(prevNumber) / 100);
        if (exp === null) {
          this.setState({ number1: prevNumber });
        } else {
          this.setState({ number2: prevNumber });
        }
        break;
      default:
        break;
    }
  };

  onExpClick = exp => {
    this.setState({ exp });
  };

  onCalc = () => {
    let { exp, number1, number2 } = this.state;

    switch (exp) {
      case EXP.plus:
        number1 = String(Number(number1) + Number(number2));
        break;
      default:
        break;
    }

    this.setState({ number1, number2: 0, exp: null });
  };

  render() {
    const { number1, number2, exp } = this.state;
    console.log(this.state);
    const currentNumber = exp === null ? number1 : number2;

    return (
      <div className="calc">
        <div className="calc-screen">{currentNumber}</div>
        <button onClick={() => this.onCtrlClick(CTRL.reset)} className="ctrl">
          C
        </button>
        <button onClick={() => this.onCtrlClick(CTRL.revert)} className="ctrl">
          +/-
        </button>
        <button onClick={() => this.onCtrlClick(CTRL.percent)} className="ctrl">
          %
        </button>
        <button onClick={() => this.onExpClick(EXP.divide)} className="exp">
          ÷
        </button>
        <button onClick={() => this.onNumberClick('7')}>7</button>
        <button onClick={() => this.onNumberClick('8')}>8</button>
        <button onClick={() => this.onNumberClick('9')}>9</button>
        <button onClick={() => this.onExpClick(EXP.multiply)} className="exp">
          ×
        </button>
        <button onClick={() => this.onNumberClick('4')}>4</button>
        <button onClick={() => this.onNumberClick('5')}>5</button>
        <button onClick={() => this.onNumberClick('6')}>6</button>
        <button onClick={() => this.onExpClick(EXP.minus)} className="exp">
          -
        </button>
        <button onClick={() => this.onNumberClick('1')}>1</button>
        <button onClick={() => this.onNumberClick('2')}>2</button>
        <button onClick={() => this.onNumberClick('3')}>3</button>
        <button onClick={() => this.onExpClick(EXP.plus)} className="exp">
          +
        </button>
        <button onClick={() => this.onNumberClick('0')} className="zero">
          0
        </button>
        <button onClick={() => this.onNumberClick('.')}>.</button>
        <button onClick={this.onCalc} className="exp">
          =
        </button>
      </div>
    );
  }
}
