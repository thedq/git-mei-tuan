import React from "react";
 export default class Constructor extends React.Component {
   // constructor小写
    constructor(props) {
        // 继承父类的属性
        super(props);
        //初始化state
        this.state = {
            a:123
        }
        //1.绑定当前监听事件
       // this.onClick=this.onFucus.bind(this);
        console.log('constructor',this);
     }
    //  2.箭头函数：相当于绑定了this
     onFucus=()=>{
        //  判断this是否为undefined
        console.log('type of:'+typeof this);//type of:object
        //console.log(this.state)
         
     }
    
    render() {
        const{a}=this.state;
        //console.log(this);//当前对象
        //3.()=>this.onFucus
        return (<div onClick={()=>this.onFucus}>Constructor {a}</div>)
    }
}