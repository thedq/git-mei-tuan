import React from 'react';

import FetchUser from '../FetchUrl';

export default class FetchUrlFromId extends React.Component {
  state = {
    id: 9527,
    user: {},
  };

  onIdChange = () => {
    this.setState({ id: this.state.id + 1 });
  };

  onGetUser = user => {
    console.log('user', user);
    this.setState({ user });
  };

  render() {
    const { id, user } = this.state;
    return (
      <>
        <button onClick={this.onIdChange}>Change User {id}</button>
        <FetchUser id={id} user={user} onGetUser={this.onGetUser} />
      </>
    );
  }
}
