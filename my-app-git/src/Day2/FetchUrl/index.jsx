import axios from 'axios'
import React from 'react'

export default class FetchUrl extends React.Component {
	static defaultProps = {}

	state = {
		user: { username: '灰度发布' },
	}

	getUser() {
		const { id, onGetUser } = this.props
		axios.get(`https://xiaozhu.run/api/user/${id}`).then((resp) => {
			console.log('resp', resp.data.data)
			const user = resp.data.data
			this.setState({ user: user })
			onGetUser && onGetUser(user)
		})
	}

	shouldComponentUpdate() {
		console.log('shouldComponentUpdate....')
		return true
	}

	render() {
		console.log('render....')
		const { user } = this.state
		return (
			<>
				<div>
					<p>ID: {user.id}</p>
					<p>Username: {user.username}</p>
				</div>
			</>
		)
	}

	componentDidMount() {
		console.log('componentDidMount...')
		axios.get('https://xiaozhu.run/api/user/9527').then((resp) => {
			console.log('resp', resp.data.data)
			this.setState({ user: resp.data.data })
		})

	}

	componentDidUpdate(prevProps) {
		console.log('componentDidUpdate')
		if (prevProps.id !== this.props.id) {
			this.getUser()
		}
	}
}
