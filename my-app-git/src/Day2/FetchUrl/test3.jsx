import React from "react";
import axios from 'axios';
export default class FetchUrl extends React.PureComponent {
    state = {
        user: {},
    }
    //初始化数据
    constructor(props) {
        super(props);
        console.log('constructor')
    }
    //第一次渲染后调用
    componentDidMount() {
      //axios方式:箭头函数
        axios.get('https://xiaozhu.run/api/user/9527').then( (resp)=> {
            console.log('resp', resp);
            console.log('resp.data',resp.data);
            console.log('resp.data.data',resp.data.data);
            const username=resp.data.data.username;
            const{user}=this.state;
            console.log('this.state',this.state);
            user.username=username;
            const newUser={...user};
            newUser.username=username;
            this.setState({
                user:newUser
            })
        });


    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('shouldComponentUpdate');
    //     if (nextState.user === this.state.user) {
    //       return false;
    //     }
    //     return true;
    //   }
    

   //更新
    componentDidUpdate(){
        console.log('componentDidUpdate');
    }
    //渲染
    render() {
        console.log('render')
        const { user } = this.state;
        console.log('user.username',user.username);
        return <div>
            Username: {user.username}
        </div>;
    }
}