import React from "react";
//import axios from 'axios';
export default class FetchUrl extends React.PureComponent {
    state = {
        user: {},
    }
    //初始化数据
    constructor(props) {
        super(props);
        console.log('constructor')
    }
    //第一次渲染后调用
    componentDidMount() {
        console.log('componentDidMount');

        //原生fetch方式
        //then:请求成功之后的操作
        fetch('https://xiaozhu.run/api/user/9527').then(r => {
            console.log('r', r);
            console.log('resp', r.json().then(response => {
                console.log('response', response);
                this.setState({
                    user: response.data
                })
            }));

        })

    }
    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('shouldComponentUpdate');
    //     if (nextState.user === this.state.user) {
    //       return false;
    //     }
    //     return true;
    //   }
    

   //更新
    componentDidUpdate(){
        console.log('componentDidUpdate');
    }
    //渲染
    render() {
        console.log('render')
        const { user } = this.state;
        return <div>
            Username:{user.username}
        </div>;
    }
}