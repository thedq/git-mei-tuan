import React from "react";
//import axios from 'axios';
export default class FetchUrl extends React.Component {
    state = {
        user: {},
    }
    componentDidMount() {
        //axios方式
        // const resp=axios.get('https://xiaozhu.run/api/user/9527').then(function (response) {
        //     console.log('resp', resp);
        // console.log(response);
        //this.setState({user: response.data.data});
        // });


        //原生fetch方式
        //then:请求成功之后的操作
        fetch('https://xiaozhu.run/api/user/9527').then(r => {
            console.log('r', r);
            console.log('resp', r.json().then(response => {
                console.log('response',response);
                this.setState({
                    user: response.data
                })
            }));
         
        })

    }

    render() {
        const { user } = this.state;
        return <div>
            Username:{user.username}
        </div>;
    }
}