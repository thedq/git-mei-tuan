import React from "react";
import axios from 'axios';
export default class FecthUsers extends React.Component {
    state = {
        user1: {},
        user2: {},
    }
    static defaultProps= {
        id:9527,
    }
    componentDidMount() {
        const {id}= this.props;
        axios.get(`https://xiaozhu.run/api/user/${id}`).then((resp) => {
            console.log('resp', resp);
            this.setState({
                user1: resp.data.data,
            })
        })
        axios.get(`https://xiaozhu.run/api/user/${id+1}`).then((resp) => {
            console.log('resp', resp);
            this.setState({
                user2: resp.data.data,
            })
        })
    }
    render() {
        const { user1, user2 } = this.state;
        return (<div>
            <div>{user1.id} Username:{user1.username}</div>
            <div>{user2.id} Username:{user2.username}</div>
        </div>);
    }
}