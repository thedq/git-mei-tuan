import React from "react";
function windowWidthHoc(Component) {
    class WrappedComponent extends React.Component{
        state={
            windowWidth:window.innerWidth
        };

        getWidth=() => {
            const windowWidth = window.innerWidth;
            this.setState({windowWidth})
        }

        componentDidMount() {
            window.addEventListener("resize",this.getWidth)
        }

        componentWillUnmount(){
            window.removeEventListener("resize",this.getWidth)
        }


        render() {
            const {windowWidth} = this.state;
            return <Component {...this.props} windowWidth={windowWidth}/>
        }
    }
    return WrappedComponent
}

export default windowWidthHoc;