import React from 'react';
import _ from 'lodash';

//
function windowWidthHoc(Component) {
  class WrappedComponent extends React.Component {
    state = {
      windowWidth: window.innerWidth,
    };

    constructor(props) {
      super(props);
      //
      this.getWidth = _.debounce(this.getWidth, 200);
      // const fn = this.getWidth.bind();
    }

    getWidth = () => {
      const windowWidth = window.innerWidth;
      this.setState({ windowWidth });
    };

    componentDidMount() {
      window.addEventListener('resize', this.getWidth);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.getWidth);
    }

    render() {
      console.log('render');
      const { windowWidth } = this.state;
      return <Component {...this.props} windowWidth={windowWidth} />;
    }
  }
  return WrappedComponent;
}

export default windowWidthHoc;
