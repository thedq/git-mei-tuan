/* 多人点餐系统 */
import Login from './Login';
import Foods from './Foods';
import Checkout from './Checkout';
import './index.css';
import React, { useContext, useState } from "react";

// 创建Context
const UseContext = React.createContext();
//解构赋值
const { Provider, Consumer } = UseContext;

export default function Order() {

  return (
    <div className="order">
        <Login />
        <Foods />
        <Checkout />
    </div>
  );
};



