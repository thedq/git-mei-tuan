/* 结算 */
import React, { useState, UseContext, useEffect } from 'react';
import './index.css';
import axios from 'axios';
import { useFoods, useOrders } from '../store';
function Checkout() {
  const [{orderData:orderData},{orderSum:orderSum}] = useOrders();
  const [isShow, setIsShow] = useState(false);
  const [{foodList:foodList}] = useFoods();
  return (
    <div className="order-bottom-cart">
      {isShow && (
        <div className="order-bottom-cart-detail">
          {orderData &&
            Object.keys(orderData).map((item, index) => {
              /* 用户名 */
              return (
                <div className="order-bottom-cart-detail-item" key={`order-bottom-cart-detail-item`+index} >
                  <h4> {item}</h4>
                  { /* 总金额 */}
                  <b>¥400</b>
                  <ul>
                    {Object.keys(orderData[item]).map((order, i) => {
                      return (

                        <li>
                          { /* 菜品名称 */}
                          <span>{order}</span>
                          <i> x </i>
                          { /* 菜品数量 */}
                          <em key={`order-bottom-cart-detail-item-em`+i}>{orderData[item][order]}</em>
                        </li>

                      );

                    })

                    }
                  </ul>
                </div>);
            })
          }

        </div>)
      }
      <div className="order-bottom-cart-control" onClick={() => setIsShow(!isShow)}>
        <i>
          <img src="http://www.xiaozhu.run/static/media/order.ef866849.svg" alt="es-lint want to get" />
          {/* 菜品总数量 */}
          <em>{orderSum}</em>
        </i>
        {/* 总价格 */}
        <b>¥{ }</b>
        <button>去结算</button>
      </div>
    </div >
  );
}

export default Checkout;
