/* 菜品 */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './index.css';
import { useFoods } from '../store';
const onScrollCategory = (id) => {
  // if (id) {
  //   let anchorElement = document.getElementById(id);
  //   if(anchorElement) {
  //     anchorElement.scrollIntoView(
  //       {behavior: 'smooth'}
  //     );
  //   }
  // }

};



export default function Foods() {
  const [{foodCategoryList:foodCategoryList}, {foodList:foodList}, {loading:loading},{ addFood:addFood}, {removeFood:removeFood}] = useFoods();
  const [isClick, setIsClick] = useState(0);
  return (
    <div className="order-category">
      {loading && <div>加载菜品中...</div>}
      <div className="order-category-list">
        <ul>
          {foodCategoryList &&
            foodCategoryList.map((category, index) => {
              return (
                <li
                  className={`.order-category-list ul li ${index === isClick ? "active" : ""
                    }`}
                  onClick={() => {
                    setIsClick(index);
                  }}
                  key={`.order-category-list-ul-li` + index}
                >
                  {category.name}
                </li>
              );
            })
          }
        </ul>
      </div>
      <div className="order-food-list">
        {foodCategoryList &&
          foodCategoryList.map((category, index) => {
            return (<div className="order-food-list-group" data-category-id={category.id} key={`order-food-list-group` + index} >
              <h3 >{category.name}</h3>
              {Array.from(foodList) &&
                Array.from(foodList).map((food, index) => {
                  return (
                    <ul key={`order-food-list` + index}>
                      <li>
                        <div className="order-food-list-image">
                          <img
                            src={food.image} alt="es-lint want to get" />
                        </div>
                        <div className="order-food-list-meta">
                          <h3>{food.name}</h3>
                          <em>￥{food.price}</em>
                        </div>
                        <div className="order-food-list-control">
                          <i onClick={() => removeFood(index)}>-</i>
                          <b key={`order-food-list-b` + index}>{food.num|| 0}</b>
                          <i onClick={() => addFood(index)}>+</i>
                        </div>
                      </li >
                    </ul>);
                })
              }
            </div>);
          })
        }
      </div>
    </div >);
};

