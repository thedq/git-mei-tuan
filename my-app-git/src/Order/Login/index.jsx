/* 登录 */
import React, { useState, useEffect, useContext, useReducer } from 'react';
import './index.css';

//登录组件
export default function Login() {
  const [isDisplay, setIsDisplay] = useState(false);
  const [context, setContext] = useState('');
  const [user, setUser] = useState('');
  const [value,setValue] = useState('');
  //登录按钮
  const onLogin = () => {
    setIsDisplay(true);
  }
 
 
  //退出按钮
  const onExit = () => {
    setIsDisplay(false);
    setUser('');

  }
  //确定按钮
  const onComfort = value => {
    /* 用户名长度 4~20，包含小写字母、数字、_、- */
    if (/^[a-z0-9\-_]{4,20}$/.test(String(value))) {
      setContext('');
      setIsDisplay(false);
      setUser(value);
    } else {
      setContext('用户名长度 4~20，包含小写字母、数字、_、-');
    }
  }

  //取消按钮
  const onCancel = () => {
    setUser('');
    setContext('');
    setIsDisplay(false);
  }

  
  return (
    <div>
      <div className="order-login">
        <div className="order-login-user">
          {user === '' && <a onClick={onLogin}>登录</a>}
          {user !== '' &&
            [
              <img src="http://www.xiaozhu.run/static/media/person.41f311b4.svg" alt="es-lint want to get" key="order-login-user-1"/>,
              <h2 key="order-login-user-2">{user}</h2>,
              <a key="order-login-user-3" onClick={onExit} > 退出</a>
            ]
          }
        </div>
      </div>
      {
        isDisplay &&
        <div className="order-login-modal" tabIndex="1">
          <div className="order-login-modal-content">
            <h3>登录</h3>
            <div className="order-login-modal-content-form">
              <input type="input" placeholder="请输入用户名" onChange={e => setValue(e.target.value)} />
              <p>{context}</p>
            </div>
            <div className="order-login-modal-content-control">
              <button onClick={onCancel}>取消</button>
              <button onClick={()=>onComfort(value)}>确定</button>
            </div>
          </div>
        </div>
      }
    </div>);
}

